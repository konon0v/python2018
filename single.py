class Single:
    __instance = None

    @staticmethod
    def getInstance():
        if Single.__instance == None:
            Single()
        return Single.__instance 

    def __init__(self):
        if Single.__instance != None:
            raise Exception("This object can be created just once!")
        else:
            Single.__instance = self


s = Single() 
print s
Single()