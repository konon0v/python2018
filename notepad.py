import Tkinter
from Tkinter import *
from ScrolledText import *
import tkFileDialog
from tkFileDialog import askopenfilename, asksaveasfilename
import tkMessageBox

root = Tkinter.Tk(className="HW 3 Notepad")
textPad = ScrolledText(root, width=100, height=80)

def open_command():
        file = tkFileDialog.askopenfile(parent=root,mode='rb',title='Select a file')
        if file != None:
            contents = file.read()
            textPad.insert('1.0',contents)
            file.close()

def save_command():
    file = tkFileDialog.asksaveasfile(mode='w')
    if file != None:
        data = textPad.get(1.0, END)
        file.write(data)
        file.close()

        
def exit_command():
    if tkMessageBox.askokcancel("Quit", "Do you really want to quit?"):
        root.destroy()

def about_command():
    label = tkMessageBox.showinfo("About", "HW 3 Notepad\nv1.0\nRoman Kononov")
        

menu = Menu(root)
root.config(menu=menu)
filemenu = Menu(menu)
menu.add_cascade(label="File", menu=filemenu)
filemenu.add_command(label="Open...", command=open_command)
filemenu.add_command(label="Save", command=save_command)
filemenu.add_separator()
filemenu.add_command(label="Close", command=exit_command)
helpmenu = Menu(menu)
menu.add_cascade(label="Help", menu=helpmenu)
helpmenu.add_command(label="About...", command=about_command)

#
textPad.pack()
root.mainloop()